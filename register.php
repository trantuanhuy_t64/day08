<?php
    session_start() ;
    date_default_timezone_set('Asia/Ho_Chi_Minh') ;
    $name = $gender = $department = $date = $address = "" ;
    $valid = true ;
    $gender_arr = array(
        0 => 'Nam',
        1 => 'Nữ'
    );    
    $departments = array(
        'MAT' => 'Khoa học máy tính',
        'KDL' => 'Khoa học vật liệu'
    );
    echo "<!DOCTYPE html>
        <html lang='vn' >
            <head>
                <meta charset='UTF-8'>
                <link href='style.css?version=1' rel='stylesheet'></link>
            </head>
	        <body>
            <fieldset> " ;
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = test_input($_POST["name"]);
                $gender = test_input(@$_POST["gender"]);
                $department = test_input($_POST["department"]);
                $date = test_input($_POST["date"]);
                $address = test_input($_POST["address"]);
                if ($name == "") {
                    echo "<font color='red'>Hãy nhập tên.</font> </br>"; 
                    $valid = false ;
                }
                if ($gender == "") {
                    echo "<font color='red'>Hãy chọn giới tính.</font> </br>"; 
                    $valid = false ;

                }
                if ($department == "") {
                    echo "<font color='red'>Hãy chọn phân khoa.</font> </br> "; 
                    $valid = false ;

                }
                if ($date == "") {
                    echo "<font color='red'>Hãy nhập ngày sinh.</font> </br>"; 
                    $valid = false ;

                }
                if ($date != "") {
                    $date_arr  = explode('/', $date);
                    if (count($date_arr) != 3 || !checkdate($date_arr[1], $date_arr[0], $date_arr[2])) {
                        echo "<font color='red'>Hãy nhập ngày sinh đúng định dạng.</font> </br>";
                        $valid = false ;
                    }
                };
                $file=$_FILES['fileupload'] ?? null;
                $filePath = '';
        
                if (!is_dir('upload')) {
                    mkdir('upload');
                }
                if (empty($file['tmp_name'])) {
                    echo "<font color='red'>Hãy chọn đúng ảnh .</font> </br>";
                    $valid = false ;
                }
                else {
                    $path_parts = pathinfo($file['tmp_name']);
                    if (getimagesize($file['tmp_name']) != False) {
                        $birth = date("YmdHis");
                        $arr=str_split($file["name"],strrpos($file["name"],"."));
                        $filePath = 'upload/'.$arr[0].'_'.$birth.$arr[1] ;
                        mkdir(dirname($filePath));
                        move_uploaded_file($file['tmp_name'],$filePath);
                    }
                    else {
                        echo "<font color='red'>Hãy chọn đúng ảnh .</font> </br>";
                        $valid = false ;
                    }
                }
            

                if ($valid == true ) {
                    $test;
                    if($_POST["gender"] == '0'){
                        $test = 'Nam';
                    }else {
                        $test = 'Nữ';
                    }
                    $_SESSION["name"] = test_input($_POST["name"]);
                    $_SESSION["gender"] = test_input($test);
                    $_SESSION["department"] = test_input($_POST["department"]);
                    $_SESSION["date"] = test_input($_POST["date"]);
                    $_SESSION["address"] = test_input($_POST["address"]);
                    $_SESSION["files"] = $filePath;
                    header('location:verify.php') ;
                }
            }
              function test_input($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
              }
          echo "
		        <form action='register.php' method='post' enctype='multipart/form-data'>
			        <div>
				        <label for = 'pwd' class='label' left=100%>Họ và tên<font color=red>*</font></label>
				        <input type='text' class='input' name='name' > <br>
				        <label for='pwd' class='label'>Giới tính<font color=red>*</font></label> " ;
					    for ($i = 0; $i < count($gender_arr); $i++) {
                            echo "
                                <input type='radio' class='gender' name='gender' value=" . $i ."> $gender_arr[$i] 
                            " ;
                        } 
                    echo "
                        <br>
                        <label for='pwd' class='label'>Phân khoa<font color=red>*</font></label> 
                        <select class= 'select' name='department'>
                            <option></option>"; 
                            foreach($departments as $department){
                                echo "<option>
                                $department
                                </option>";
                            }
                    echo "
                        </select> <br>
                        <label for='pwd' class='label'>Ngày sinh<font color=red>*</font></label>
                        <input placeholder='dd/mm/yyyy' type='text' class='date' name='date'/>  <br>
                        <label for='pwd' class='label'>Địa chỉ</label>
                        <input type='text' class='input' name='address' > <br>
                        <label for='pwd' class='label'>Hình ảnh</label>
                        <input type='file' name='fileupload' class='file' id='fileupload'>
                        <br>
				        <button type='submit' class='button' name='dangnhap'>Đăng ký</button>
			        </div>
		        </form>
            </fieldset>
	        </body>
        </html> " ;
?>